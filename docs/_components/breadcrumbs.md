---
permalink: /breadcrumbs
layout: left_sidebar
sidebar: components
title: Breadcrumbs
---

## Notes on Implementation

We do not use Bootstrap's Breadcrumb component, but implement our own with increased color contrast and including the greater-than character between links to make the link hierarchy clear to visual users.

## Accessibility Notes

Breadcrumb navigation can greatly enhance the users ability to navigate and find their place within a website or web application.

To prevent screen reader announcement of the visual separators between links, they are added via CSS.

Since breadcrumbs provide a navigation, it is recommended to add a meaningful label such as `aria-label="breadcrumb"` to identify the type of navigation provided in the `<nav>` element, as well as applying an `aria-current="page"` to the last item of the breadcrumb trail to indicate that it represents the current page.

For more information on implementing accessible breadcrumbs, see the [WAI-ARIA Authoring Practices for the breadcrumb pattern](https://www.w3.org/TR/wai-aria-practices/#breadcrumb)

## Example

{% capture example %}

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
    </ol>
</nav>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Library</li>
    </ol>
</nav>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Library</a></li>
        <li class="breadcrumb-item active" aria-current="page">Data</li>
    </ol>
</nav>
{% endcapture %}

{% include example.html content=example myVal="using breadcrumbs" myBtn="first" hiddenSection="hiddenFirst" %}
