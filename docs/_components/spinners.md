---
permalink: /spinners
layout: left_sidebar
sidebar: components
title: Spinners
---

## Notes on Implementation

We do use Bootstrap's Spinner component, but adjust the duration and number of iterations so the movement is slower and is completed in under 5 seconds.

## Accessibility Notes

According to the [Understanding Docs for WCAG Success Criterion 2.2.2](https://www.w3.org/WAI/WCAG21/Understanding/pause-stop-hide.html#examples:~:text=A%20%22loading%22%20animation): "A preloader animation is shown on a page which requires a certain percentage of a large file to be downloaded before playback can begin. The animation is the only content on the page and instructs the user to please wait while the video loads. Because the moving content is not presented in parallel with other content, no mechanism to pause, stop or hide it needs to be provided, even though the animation may run for more than 5 seconds for users with slower connections."

### Describing a Loading Progress

- The element containing the spinner should have an accessible name to describe what is happening (such as "Loading..." as in examples below). The `sr-only` class can be used to hide this description from visual users.
- Add the `aria-busy` attribute to the loading region and set it to true.
- Update or remove this announcement once the loading is complete.

## Example of Border Spinner

{% capture example %}

<div class="spinner-border text-primary" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-border text-secondary" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-border text-success" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-border text-danger" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-border text-warning" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-border text-info" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-border text-light" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-border text-dark" role="status">
  <span class="sr-only">Loading...</span>
</div>

{% endcapture %}
{% include example.html content=example myVal="using border spinner" myBtn="borderSpinner" hiddenSection="hiddenBorderSpinner" %}

## Example of Growing Spinner

{% capture example %}

<div class="spinner-grow text-primary" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-secondary" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-success" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-danger" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-warning" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-info" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-light" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-dark" role="status">
  <span class="sr-only">Loading...</span>
</div>
{% endcapture %}
{% include example.html content=example myVal="using growing spinner" myBtn="growingSpinner" hiddenSection="hiddenGrowingSpinner" %}
