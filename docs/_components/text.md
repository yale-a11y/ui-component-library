---
permalink: /text
layout: left_sidebar
sidebar: components
title: Text
---

## Notes on Implementation

There are several differences between Bootstrap's text utilities and the Yale Component Library's:

- We offer additional font family classes:
  - `.text-monospace`
  - `.text-font-family-sans-serif`
  - `.text-font-family-sans-italic`
  - `.text-font-family-sans-medium`
  - `.text-font-family-sans-bold`
  - `.text-font-family-serif`
  - `.text-font-family-serif-bold`
  - `.text-font-family-serif-italic`
  - `.text-font-family-serif-bold-italic`
- To generate text color variants, we use our own mixin, `yale-text-emphasis-variant` rather than Bootstrap's `text-emphasis-variant`. Our variant tries to ensure sufficient color contrast accessibility.
- We do not include `.text-black-50` or `.text-white-50` from Bootstrap.
- We add `.text-base-size`, which resets the text's font size and line height.

## Examples

### Font Utilities

{% capture example %}
{% for utility in site.data.text-utilities %}

<h4> Font class="{{ utility.name }}"</h4>
<p class="{{ utility.name }}">This is a text utility style based on the <strong>{{utility.name}}</strong> class. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis sapien lorem, rhoncus ultricies orci iaculis vitae. Phasellus ipsum erat, imperdiet non porttitor vitae, eleifend sed ex.</p>
{% endfor %}
{% endcapture %}
{% include example.html content=example myVal="text utilities" myBtn="textUtilities" hiddenSection="hiddenTextUtilities" %}

### Text Emphasis

{% capture example %}
{% for color in site.data.theme-colors %}

<h4>Text Emphasis class="text-{{ color.name }}"</h4>
    <p class="text-{{ color.name }}">This is text that is styled with the text-{{ color.name }} class and <a href="#">this is an example link</a> to see</p>
{% endfor %}
{% endcapture %}
{% include example.html content=example myVal="text emphasis" myBtn="emphasis" hiddenSection="hiddenEmphasis" %}
