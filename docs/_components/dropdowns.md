---
permalink: /dropdowns
layout: left_sidebar
sidebar: components
title: Dropdowns
---

## Notes on Implementation

We implement [Bootstrap's Dropdowns component](https://getbootstrap.com/docs/4.3/components/dropdowns/), but remove the `aria-haspopup="true"` attribute on the button element.

## Accessibility Notes

Remove the `aria-haspopup="true"` attribute on the Bootstrap examples. If the dropdown includes more than one link, they should be marked up as a list (using `<ul>` and `<li>` elements). As with the Collapse component, it is important that this dropdown be a button with an accessible name that is unique and descriptive of its function. The state of the element must be conveyed via the aria-expanded attribute. See the example below for how to implement.

## Example

{% capture example %}

<div class="dropdown">
  <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
    Dropdown
  </button>
  <div class="dropdown-menu">
  <ul class="nav flex-column">
     <li><a class="dropdown-item" href="#">Action</a></li>
     <li><a class="dropdown-item" href="#">Another action</a></li>
     <li><a class="dropdown-item" href="#">Something else here</a></li>
    </ul>
  </div>
</div>

{% endcapture %}

{% include example.html content=example myVal="using dropdowns" myBtn="first" hiddenSection="hiddenFirst" %}
