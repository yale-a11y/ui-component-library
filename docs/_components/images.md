---
permalink: /images
layout: left_sidebar
sidebar: components
title: Images
---

## Notes on Implementation

We use Bootstrap's `_images.scss` file with minor tweaks to ensure color contrast. The documentation for [Bootstrap Images](https://getbootstrap.com/docs/4.3/content/images/) is applicable.

## Accessibility Notes

- All `<img>` tags <strong>must</strong> have `alt` attributes.
- In order for images to be accessible to assistive technologies, a text alternative must be provided in the `alt` attribute. The alt text should be brief, should not include phrases like "image of", and should convey critical information about the meaning of the image to an assistive technology user. Consider the following resources for how to write good alt text:
  - [Alt-texts: The Ultimate Guide by Access Lab](https://axesslab.com/alt-texts/)
  - [Alternative Text by WebAIM](https://webaim.org/techniques/alttext/)
  - [Image Concepts Tutorial from the WAI](https://www.w3.org/WAI/tutorials/images/)
- Images of text should be avoided. If they are used, the alt text should match the visual text exactly.
- Image links <strong>must</strong> have alt text describing the destination of the link.
- Decorative images <strong>should</strong> have a blank alt attribute (`alt=''`).
- Avoid using the `title` attribute in general.
- For complex images, charts, graphs, posters, and the like, including short alt text and a long description of the information contained in the image immediately below the image.
- Reduce redundant text. Frequently websites will have visual design elements that combine images, headings, and surrounding text as links to other pages, such as list of news articles. In these cases, developers <strong>should</strong> combine all of these elements into a single link, rather than implementing multiple adjacent links to the same destination. Doing so reduces repetition by assistive technologies, which can be annoying for users. In these cases, it is generally best to give the `img` tag a null `alt` attribute.
  - For example:

### Avoid

<pre><code>&lt;div&gt;
    &lt;a href=&quot;...&quot;&gt;&lt;img src=&quot;...&quot; alt=&quot;My News story&quot;&lt;/a&gt;
    &lt;h3&gt;&lt;a href=&quot;...&quot;&gt;My News Story&lt;/a&gt;&lt;/h3&gt;
&lt;/div&gt;
</code></pre>

### Best

<pre><code>&lt;div&gt;
    &lt;a href=&quot;...&quot;&gt;
        &lt;img src=&quot;...&quot; alt=&quot;&quot;&gt;
        &lt;h3&gt;My News Story&lt;/h3&gt;
    &lt;/a&gt;
&lt;/div&gt;
</code></pre>
