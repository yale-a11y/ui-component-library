---
permalink: /progress
layout: left_sidebar
sidebar: components
title: Progress
---

## Notes on Implementation

We do not use Bootstrap's Progress component. We implement our progress component with fewer features and include a border to increase contrast. In particular, we <strong>do not</strong> implement the multiple bars or the animation for the progress component. Otherwise, the documentation for [Bootstrap Progress](https://getbootstrap.com/docs/4.3/components/progress/) is applicable.

## Accessibility Notes

Include the role of "progressbar" on the element for browsers to announce to assistive technology the role of this element. The progress bar element must have the attributes of aria-valuenow, aria-valuemin, and aria-valuemax, with appropriate values. The aria-valuenow attribute and the style width will need to update dynamically as the user steps through the process that the progress bar is measuring.

### Accessible Label

An aria-label should be used to label the progress bar to users of assistive technology. For example, if the progress bar is for a quiz, include an aria-label="quiz" (AT user will hear "quiz progress bar"). If there is no aria-label, AT users that are at this element will hear "progress bar".

### Describing a Loading Progress

If the progressbar is describing the loading process of a particular region of a page:

- Add the aria-describedby attribute to the (loading) region and set it to the id of the progressbar
- Add the aria-busy attribute to the same (loading) region and set it to true.
- Update or remove these attributes once the loading is complete.

## Example without Visual Labels

{% capture example %}

 <div class="progress mb-3">
     <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
 </div>
 <div class="progress mb-3">
     <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0"
         aria-valuemax="100"></div>
 </div>
 <div class="progress mb-3">
     <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0"
         aria-valuemax="100"></div>
 </div>
 <div class="progress mb-3">
     <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0"
         aria-valuemax="100"></div>
 </div>
 <div class="progress mb-3">
     <div class="progress-bar" role="progressbar" style="width: 100%"  aria-valuenow="100" aria-valuemin="0"
         aria-valuemax="100"></div>
 </div>

{% endcapture %}
{% include example.html content=example myVal="using progress bars" myBtn="progress" hiddenSection="hiddenProgress" %}

## Example with Visual Labels

{% capture example %}

 <div class="progress mb-3">
     <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
 </div>
 <div class="progress mb-3">
     <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0"
         aria-valuemax="100">25%</div>
 </div>
 <div class="progress mb-3">
     <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0"
         aria-valuemax="100">50%</div>
 </div>
 <div class="progress mb-3">
     <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0"
         aria-valuemax="100">75%</div>
 </div>
 <div class="progress mb-3">
     <div class="progress-bar" role="progressbar" style="width: 100%"  aria-valuenow="100" aria-valuemin="0"
         aria-valuemax="100">100%</div>
 </div>

{% endcapture %}
{% include example.html content=example myVal="using progress bars with visual labels" myBtn="progressWithLabels" hiddenSection="hiddenProgressLabels" %}
