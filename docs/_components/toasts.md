---
permalink: /toasts
layout: left_sidebar
sidebar: components
title: Toasts
---

## Notes on Implementation

We do not recommend the use of Bootstrap's Toast component, but instead suggest to use [Dismissible Alerts]({{'/alerts' | relative_url }}). Dismissible Alerts provide the same function as the Toast component, but are not timed which allows users an adequate amount of time to view the message without it being timed-out.

## Accessibility Notes

If a toast is preferred over a dismissable alert, the documentation for [Bootstrap Toasts](https://getbootstrap.com/docs/4.3/components/toasts/) can be consulted for further accessibility information. Toasts are non-actionable notifications designed to mimic push notifications used by desktop and mobile operating systems. The default time-out of toasts may not be suitable for users of assistive technology, who may not not have time to view the message before the time-out. Dismissable alerts allow for users to dismiss the message after viewing to ensure any relevant information is viewed.
