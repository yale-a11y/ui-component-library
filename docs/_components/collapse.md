---
permalink: /collapse
layout: left_sidebar
sidebar: components
title: Collapse/Accordion
---

## Notes on Implementation

We implement Bootstrap's Accordion component styles from Bootstrap 5. Bootstrap 4's accordion implementation (an example within their Collapse component) is missing the necessary styles (such as the icon to indicate it can be opened). However, the functionality from Bootstrap 4's Collapse component is used.

## Accessibility Notes

In the example below, taken from Bootstrap, we remove the `data-parent="#selector"` from each section so that more than one section can be opened at a time. We also add an arrow indicating that this section can be expanded.

This interactive component should be a `<button>` wrapped within a heading element. The accessible name of the heading should be unique and make sense out of context. The heading level should chosen to appropriately follow the structure on the page. And the state must be included using the `aria-expanded` attribute. Follow the example below.

## Example

{% capture example %}

<div class="mb-4 pb-3" id="accordionExample">
  <div class="accordion">
    <div class="accordion-item" id="headingOne">
      <h3 class="accordion-header">
        <button class="accordion-button collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
          Collapsible Group Item #1 
        </button>
      </h3>
    </div>

    <div id="collapseOne" class="collapse accordion-collapse">
      <div class="accordion-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>

  </div>
  <div class="accordion">
    <div class="accordion-item" id="headingTwo">
      <h3 class="accordion-header">
        <button class="accordion-button collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Collapsible Group Item #2
        </button>
      </h3>
    </div>
    <div id="collapseTwo" class="collapse accordion-collapse">
      <div class="accordion-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
  <div class="accordion">
    <div class="accordion-item" id="headingThree">
      <h3 class="accordion-header">
        <button class="accordion-button collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Collapsible Group Item #3
        </button>
      </h3>
    </div>
    <div id="collapseThree" class="collapse accordion-collapse">
      <div class="accordion-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
  
</div>
{% endcapture %}

{% include example.html content=example myVal="using collapse" myBtn="first" hiddenSection="hiddenFirst" %}
