---
permalink: /buttons
layout: left_sidebar
sidebar: components
title: Buttons
---

## Notes on Implementation

We do not use Bootstrap's Buttons component, but implement our own with increased color contrast.

## Accessibility Notes

If a link performs the same action as a button, giving the element `role="button"`
helps assistive technology users understand the function of the element.
However, it is recommended to use native HTML buttons rather than the
button role, as native HTML buttons provide keyboard and focus requirements
by default and are best supported by assistive technologies.

## Examples

### Default Button Styles

A basic button can be created with the `.btn` class. You can use several predefined button styles, each serving its own semantic purpose.

{% capture example %}
{% for color in site.data.theme-colors %}
<button type="button" class="btn btn-{{ color.name }}">{{ color.name }}</button>{% endfor %}
{% endcapture %}

{% include example.html content=example myVal="using default button styles" myBtn="first" hiddenSection="hiddenFirst" %}

### Outline Button Styles

If you don't want a background for the button, replace the default modifier class with the `.btn-outline-*`.

{% capture example %}
{% for color in site.data.theme-colors %}
<button type="button" class="btn btn-outline-{{ color.name }}">{{ color.name }}</button>{% endfor %}
{% endcapture %}

{% include example.html content=example myVal="using outline button styles" myBtn="second" hiddenSection="hiddenSecond" %}

### Disabled Button Styles

Make buttons look disabled by adding the `disabled` boolean attribute to the button tag.

It is recommended to use the html `disabled` attribute rather than a disabled class in CSS. The `disabled` attribute will make the button not respond to user action, not focusable, and the `command` event will not fire.

{% capture example %}
{% for color in site.data.theme-colors %}
<button type="button" class="btn btn-{{ color.name }}" disabled>{{ color.name }}</button>{% endfor %}
{% endcapture %}

{% include example.html content=example myVal="using disabled button styles" myBtn="third" hiddenSection="hiddenThird" %}

### Other Button Styles

{% capture example %}

<p>
    <button type="button" class="btn btn-primary btn-lg">Large button</button>
    <button type="button" class="btn btn-primary">Default button</button>
    <button type="button" class="btn btn-primary btn-sm">Small button</button>
</p>

<p>
    <button type="button" class="btn btn-primary btn-lg btn-block">Block level button</button>
</p>
{% endcapture %}
{% include example.html content=example myVal="other button styles" myBtn="other" hiddenSection="hiddenOther" %}
