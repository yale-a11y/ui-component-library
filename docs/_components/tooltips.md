---
permalink: /tooltips-popovers
layout: left_sidebar
sidebar: components
title: Tooltips and Popovers
---

## Notes on Implementation

We use Bootstrap’s Tooltip and Popover components. The documentation for [Bootstrap Tooltips](https://getbootstrap.com/docs/4.3/components/tooltips/) and [Bootstrap Popovers](https://getbootstrap.com/docs/4.3/components/popovers/) is applicable.

## Accessibility Notes

Popovers and tooltips provide further information on an element when the user clicks or hovers. Tooltips and Popovers must be discoverable and readable with a mouse, other pointer devices, keyboard, screen reader, zoom software, and any other assistive technology. They should provide information that may be helpful in learning the UI, but is not required to operate it. When open, tooltips and popovers should not block a user from performing any other task on the screen.

### Best Practices for Tooltip and Popover Content

- **Tooltips and popovers should never contain essential content.** Assume that the content may never be read by all of your users. If your user cannot infer how to use your UI without the content, move the information out of the component and onto the page to increase discoverability.
- **Write concise tooltip and popover text.** Users who are on a small screen or with high zoom will need to move around the screen to read the content, potentially losing their place. Likewise, large chunks of text may create cognitive overload for some users.
- **Avoid rich content.** Formatting will not be conveyed to screen reader users (bold, italics, icons, etc.).
- **Do not use interactive content.** Links or buttons should not be placed inside a tooltip or popover.

## Examples

### Tooltip

Only use `data-placement="top"` and `data-placement="bottom"`. Displaying the tooltip to the left or right may overlap the button and a pointer user will not be able to dismiss on mobile or when zoomed in.
{% capture example %}

<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Tooltip content">
  Tooltip on top
</button>

<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Tooltip content">
  Tooltip on bottom
</button>

{% endcapture %}

{% include example.html content=example myVal="Tooltip bottom and top example" myBtn="tooltip" hiddenSection="hiddenTooltip" %}

### Dismissable Popover

We do not implement Bootstrap's Toggle Popover because pointer users will not be able to dismiss content on mobile or when zoomed in because the popover content overlaps the button. Instead, we implement the Dismissable Popover, which users can dismiss by clicking anywhere outside the button.
{% capture example %}

<a tabindex="0" class="btn btn-lg btn-info" role="button" data-toggle="popover" data-trigger="focus" title="Dismissible Popover" data-content="Popover content should be concise and not contain essential information."><i class="fa fa-info-circle"></i> Hint</a>

{% endcapture %}

{% include example.html content=example myVal="Dismissable popover example" myBtn="popover" hiddenSection="hiddenPopover" %}
