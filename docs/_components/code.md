---
permalink: /code
layout: left_sidebar
sidebar: components
title: Code
---

## Notes on Implementation

We use Bootstrap’s Code implementation. The documentation for [Bootstrap Code](https://getbootstrap.com/docs/4.3/content/code/) is applicable.

## Accessibility Notes

Be sure to use a text color for the code display that has good color contrast with the surrounding body text. Darker code colors will get lost and be hard for visual users to easily identify.

## Examples

### Inline Code

Wrap inline snippets of code with `<code>`. Be sure to escape HTML angle brackets.
{% capture example %}

<p>For example, <code>&lt;section&gt;</code> should be wrapped as inline.</p> 
{% endcapture %}
{% include example.html content=example myVal="Inline Code" myBtn="inlineCode" hiddenSection="hiddenInlineCode" %}

### Code Block

Use `<pre>`s for multiple lines of code. You may add the `.pre-scrollable` class, which will set a max-height of 340px and provide a y-axis scrollbar.
{% capture example %}

<pre><code>&lt;p&gt;Sample text here...&lt;/p&gt;
&lt;p&gt;And another line of sample text here...&lt;/p&gt;
</code></pre>

{% endcapture %}
{% include example.html content=example myVal="Code Block" myBtn="codeBlock" hiddenSection="hiddenCodeBlock" %}

### Variables

Use the `<var>` tag to indicate variables.
{% capture example %}

<p><var>y</var> = <var>m</var><var>x</var> + <var>b</var></p>
{% endcapture %}
{% include example.html content=example myVal="Variable Code" myBtn="variable" hiddenSection="hiddenvariable" %}

### User Input and Output

{% capture example %}

<p>To switch directories, type <kbd>cd</kbd> followed by the name of the directory.<br> To edit settings, press <kbd><kbd>ctrl</kbd> + <kbd>,</kbd></kbd></p>
<p><samp>This text is meant to be treated as sample output from a computer program.</samp></p>
{% endcapture %}
{% include example.html content=example myVal="User Input and Output" myBtn="userInputOutput" hiddenSection="hiddenUserInputOutput" %}
