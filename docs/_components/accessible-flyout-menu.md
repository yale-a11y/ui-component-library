---
permalink: /accessible-flyout-menu
layout: left_sidebar
sidebar: components
title: Accessible Flyout Menu
---

## Notes on Implementation

The Accessible Flyout Menu is a custom widget that is **not** a part of Bootstrap. The source library can be found at [https://git.yale.edu/yale-ui/accessible_flyout_menu](https://git.yale.edu/yale-ui/accessible_flyout_menu).

## Setting Up the Menu

- The wrapper around the flyout menu should be given the class `.accessible-flyout-menu`. This is the hook that the JavaScript uses to dynamically add functionality to the menu, including classes. **Note**: The widget itself should ultimately reside inside a `nav` element with an ARIA label
- The widget should be structured as a nested unordered list. The flyout menu is only intended to descend one level deep.
- The top level `ul` should a be given the class `.menu`
- Each `li` in the `ul.menu` should be given a class `.menu-item`. Each `li` with a submenu should also be given a class `.has-submenu`
- Within each `li.menu-item`, give the menu item link the class `.menu-item-link`.
- Each submenu `ul` should be given a class `.submenu` and a unique `id` attribute.
- Each submenu `li` should be given a class `.submenu-item`.
- The link inside of each `li.submenu-item` should be given the class `submenu-item-link`.

## Initializing the Menu through JavaScript

- JavaScript will add utility classes and elements to your menu.
- You must proactively call the menu build function in your JavaScript. The function to call is `window.accessibleMenu`. This function should be called after `yale-ui.js` has been loaded onto your page.
- It is important to pass a breakpoint argument into `window.accessibleMenu`. The breakpoint is the pixel value where you would wish for the menu to transition from mobile to desktop. For reference, the breakpoints for the Yale UI Component Library are:
  <ul>
    {% for item in site.data.breakpoints %}
    <li>{{ item.name }}: {{ item.size }}</li>
    {% endfor %}
  </ul>

## Creating a Mobile Menu Button

The flyout menu does not come packaged with its own layout button. Instead, you should combine with the Bootstrap [collapse](https://getbootstrap.com/docs/4.3/components/collapse/) functionality.

## Adding the Icons

The flyout menu uses Font Awesome v4.7 Icons for the mobile menu button and submenu buttons. [Add the Font Awesome CDN to your project](https://fontawesome.com/v4.7.0/).

## Example

{% capture example %}

<p>
<button class="fa fa-bars btn btn-primary d-inline-block d-lg-none" data-toggle="collapse" data-target="#navExample" aria-label="Show Navigation"></button>
</p>

<div class="collapse d-lg-block" id="navExample">
<nav class="accessible-flyout-menu" aria-label="example flyout menu">

<ul class="menu">
  <li class="menu-item">
    <a href="#" class="menu-item-link">Menu Item 1</a>
  </li>
  <li class="menu-item has-submenu">
      <a href="#" class="menu-item-link">Menu Item 2</a>
      <ul class="submenu" id="flyoutSubmenu">
        <li class="submenu-item"><a href="#" class="submenu-item-link">Submenu Item 1</a></li>
        <li class="submenu-item"><a href="#" class="submenu-item-link">Submenu Item 2</a></li>
        <li class="submenu-item"><a href="#" class="submenu-item-link">Submenu Item 3</a></li>
      </ul>
  </li>
  <li class="menu-item has-submenu">
        <a href="#" class="menu-item-link">Menu Item 3</a>
        <ul class="submenu" id="flyoutSubmenu2">
          <li class="submenu-item"><a href="#" class="submenu-item-link">Submenu Item 1</a></li>
          <li class="submenu-item"><a href="#" class="submenu-item-link">Submenu Item 2</a></li>
          <li class="submenu-item"><a href="#" class="submenu-item-link">Submenu Item 3</a></li>
        </ul>
    </li>
    <li class="menu-item">
        <a href="#" class="menu-item-link">Menu Item 4</a>
      </li>
</ul>

</nav>
</div>

{% endcapture %}
{%- include example.html content=example myVal="accessible flyout menu" myBtn="modal" hiddenSection="hiddenFlyout" -%}
