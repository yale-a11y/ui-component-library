---
layout: left_sidebar
sidebar: about
title: About
permalink: /about
---

<nav aria-label="table of contents" class="sidebar-nav d-lg-none mt-2">
  <ul>
    <li><a href="#what-is-it" class="text-font-family-sans-serif">What is it?</a></li>
    <li><a href="#how-does-it-help" class="text-font-family-sans-serif">How Does it Help?</a></li>
    <li><a href="#where-do-i-start" class="text-font-family-sans-serif">Where Do I Start?</a></li>
    <li><a href="#training" class="text-font-family-sans-serif">Training</a></li>
    <li><a href="#questions" class="text-font-family-sans-serif">Questions?</a></li>
  </ul>
</nav>

## Overview

The Yale UI Component Library is a resource designers and developers can use to make it easier to build consistent user interfaces that follow Yale’s usability, identity, and accessibility standards.

## What is it?

The Component Library is a customized version of [Bootstrap 4](https://getbootstrap.com/docs/4.3/getting-started/introduction/), one of the most popular front-end web frameworks for building responsive sites. The ITS Digital Accessibility team, which maintains the library, chose to build it on top of Bootstrap so that designers and developers with knowledge of that framework would find it easy to implement. It also allows us to take advantage of a wealth of existing documentation and [training resources](https://www.linkedin.com/learning/bootstrap-4-essential-training) available for Bootstrap.

## How Does it Help?

By using the Component Library, designers and developers can reduce the effort to come up with designs and to build components for their front-ends, resulting in reduced time to complete projects, and resulting in more consistent interfaces. It is aligned with the ITS organizational priorities to provide consistent, clear, usable, and accessible services, and helps site builders adhere to [Yale’s web accessibility policy](https://your.yale.edu/policies-procedures/policies/1605-web-accessibility-policy). By using the components provided in the library, and following the special guidance in the documentation, conformance with the [Web Content Accessibility Guidelines](https://www.w3.org/TR/WCAG21/) (WCAG) is greatly simplified.
The Component Library also assists in adhering to Yale's [Usability Best Practices](https://usability.yale.edu/usability-best-practices) and [Identity Guidelines for the web](https://yaleidentity.yale.edu/web).

## Where Do I Start?

Use one of the [starter pages and other examples]({{'/examples' | relative_url}}) to start building your front-end. View the Using Bootstrap to Build Accessible Front-Ends recording (below) of a training session to learn how to use the library and Bootstrap to build your site.

## Training

### Using Bootstrap to Create Accessible Front-Ends

- [PowerPoint Presentation](<https://usability.yale.edu/sites/default/files/files/Using%20Bootstrap%20to%20Make%20Accessible%20Front-Ends(2).pptx>)
- [Video Recording](https://youtu.be/q31zBMriDpc)

## Questions?

Email the [Accessibility Team at Yale](mailto:accessibility@yale.edu)
