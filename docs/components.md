---
layout: left_sidebar
sidebar: components
title: Components
permalink: /components
---

<nav aria-label="components" class="sidebar-nav d-lg-none mt-2">
<div class="row">
<div class="col">
 {% assign half = site.components | size | divided_by: 2 %}
  <ul>
    {% for component in site.components %}
     {% if forloop.index <=half %}
    <li><a href="{{ component.url | relative_url }}" class="text-font-family-sans-serif">{{ component.title }}</a></li>
    {% endif %}
    {% endfor %}
     </ul>
    </div>
    <div class="col">
    <ul>
     {% for component in site.components %}
     {% if forloop.index >half %}
    <li><a href="{{ component.url | relative_url }}" class="text-font-family-sans-serif">{{ component.title }}</a></li>
    {% endif %}
    {% endfor %}
  </ul>
  </div>
  </div>
</nav>

## Overview

Accessibility notes are provided for each component. Please read through these carefully and implement each component in an accessible way. If you have any questions, please [email the accessibility team](mailto:accessibility@yale.edu).

## Bootstrap Utilities

We implement Bootstrap's Utilities as is, with the exception of colors and font styles. The documentation for [Bootstrap Utilities](https://getbootstrap.com/docs/4.3/utilities/borders/) is generally applicable. Documentation for our implementation of [Background Colors]({{'/background-colors' | relative_url}}) and [Text]({{'/text' | relative_url}}) is included within the list of Components.

<div class="alert alert-danger">
<h3 class="alert-heading">Warning</h3>
Utilities are very powerful, as they generally use the CSS <code>!important</code> declaration. In general, CSS <code>!important</code> should be avoided wherever possible, as it may create technical debt, have unintended consequences, and break accessibility. Developers <strong>must</strong> ensure their code is accessible when using utility classes. Developers should extend the UI library, rather than use text utilities, where possible.
</div>
