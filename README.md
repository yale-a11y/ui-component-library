# Yale UI Component Library

The Yale UI Component Library was made to help developers build interfaces that match Yale's visual identity, and follow WCAG 2 AA accessibility recommendations.

- [Source](https://gitlab.com/yale-a11y/ui-component-library)
- [Documentation Website](https://yale-a11y.gitlab.io/ui-component-library/)
- [Yale's Identity Guidelines for the web](https://yaleidentity.yale.edu/web)
- [Usability & Web Accessibility](https://usability.yale.edu/)

## Table of Contents

- [Getting Started](#using-the-compiled-assets)
- [Adding Yale Fonts](#adding-yale-fonts)
- [Documentation](#documentation)
- [Working with Source Files](#working-with-source-files)
- [Contributing](#contributing)
- [Extending](#extending-the-ui-component-library)
- [Dependencies](#libraries-and-dependencies)
- [Changelog](#version-history-and-changelog)

## Using the Compiled Assets

1. Download the compiled assets at <a href="https://yale-a11y.gitlab.io/ui-component-library/">the documentation site</a> OR install the compiled assets via npm with
<pre><code>npm install yale-ui-component-library</code></pre>

Within the folder, you'll find the following directories with the compiled and minified CSS and JavaScript:

<pre><code>ui-component-library/
   +-- css/
       +-- yale-ui.css
       +-- yale-ui.min.css
   ¦
   +-- js/
       +-- yale-ui-scripts.js
       +-- yale-ui-scripts.min.js
</code></pre>

2. Add the CSS and JS to your project's asset folder.

3. Include the CSS and JS as part of your project. (Note: You'll have to also include the jQuery, Popper.js, and Bootstrap scripts separately and in that order.)

<pre><code>&lt;head&gt;
  &lt;!-- ... --&gt;
  &lt;link rel=&quot;stylesheet&quot; href=&quot;assets/css/yale-ui.css&quot;&gt;
  &lt;!-- ... --&gt;
&lt;/head&gt;
&lt;body&gt;
  
  &lt;!-- ... --&gt;
  
  &lt;script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous";&gt;&lt;/script&gt;
  &lt;script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"&gt;&lt;/script&gt;
  &lt;script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"&gt;&lt;/script&gt;
  &lt;script src=&quot;assets/js/yale-ui-scripts.js&quot;&gt;&lt;/script&gt;
&lt;/body&gt;
</code></pre>

## Adding Yale Fonts

The UI Component Library uses the Mallory and YaleNew fonts, with Verdana being the fallback font for Mallory and Georgia being the fallback font for YaleNew.
[Information on Mallory and YaleNew fonts](https://yaleidentity.yale.edu/typefaces)

### Mallory

A limited license for Mallory OpenType fonts may be requested by designated Yale graphic designers and vendors for Yale projects. Mallory Web fonts may be used for official Yale Web sites.

To add the Mallory font to your project, add the font to your fonts folder and add a CSS file with the following @font-face rules:

<pre><code>
@font-face {
    font-family: "Mallory";
    src: url("fonts/Mallory/Mallory/Mallory-Light.woff") format("woff"),
         url("fonts/Mallory/Mallory/Mallory-Light.woff2") format("woff2");
       font-weight: normal;
    font-style: normal;
}
@font-face {
    font-family: "Mallory-Medium";
    src: url("fonts/Mallory/Mallory/Mallory-Medium.woff") format("woff"),
         url("fonts/Mallory/Mallory/Mallory-Medium.woff2") format("woff2");
       font-weight: normal;
    font-style: normal;
}
</code></pre>

### YaleNew

YaleNew is the official serif font for Yale websites.

To add the YaleNew font to your project, add the font to your fonts folder and add a CSS file with the following @font-face rules:

<pre><code>
@font-face {
    font-family: "yalenew";
    src: url("fonts/YaleNew/YaleNew-normal-normal/yalenew-roman-webfont.eot");
    src: url("fonts/YaleNew/YaleNew-normal-normal/yalenew-roman-webfont.eot?#iefix") format("embedded-opentype"),
         url("fonts/YaleNew/YaleNew-normal-normal/yalenew-roman-webfont.woff2") format("woff2"),
         url("fonts/YaleNew/YaleNew-normal-normal/yalenew-roman-webfont.woff") format("woff"),
         url("fonts/YaleNew/YaleNew-normal-normal/yalenew-roman-webfont.ttf") format("truetype");
    font-weight: normal;
    font-style: normal;
}
@font-face {
	font-family: "yalemarks";
	src: url("fonts/YaleNew/YaleNew-marks/yalemarks-webfont.eot");
	src: url("fonts/YaleNew/YaleNew-marks/yalemarks-webfont.eot?#iefix") format("embedded-opentype"),
		 url("fonts/YaleNew/YaleNew-marks/yalemarks-webfont.ttf") format("truetype"),
		 url("fonts/YaleNew/YaleNew-marks/yalemarks-webfont.woff") format("woff"),
		 url("fonts/YaleNew/YaleNew-marks/yalemarks-webfont.svg#yalenewmarks") format("svg");
	font-weight: normal;
	font-style: normal;
}
</code></pre>

## Documentation

The Yale UI Component Library documentation, included in the `docs/` folder, is built with Jekyll and publicly hosted on [GitLab Pages](https://yale-a11y.gitlab.io/ui-component-library/)

### Running the documentation locally

- Requirements:
  - Git
  - Jekyll
  - NodeJS
  - Gulp
  - Yarn

1. Clone the repo

```
git clone https://gitlab.com/yale-a11y/ui-component-library.git
```

2. Run `bundle` to install Jekyll for your project

3. Run `jekyll serve` to build and serve the local web server at `http://localhost:4000`

## Working With Source Files

We use Gulp to build all of our files and Yarn to handle our dependencies. If you wish to work with the source files, first run `yarn` in your command line to download the required node modules. To rebuild the project assets after working with the source files, run `gulp` in your command line.

## Contributing

We would love to have you contribute to this project! Email <a href="mailto:accessibility@yale.edu">accessibility@yale.edu</a> to discuss!

## Extending the UI Component Library

The UI Component Library was designed to be extensible. Custom development projects at Yale should extend this project where possible (and contribute improvements back to the project so that all Yale developers may benefit).

The project itself can be included in your Node.js project through such methods as `npm install <url>` or `yarn add <url>`. The UI Component Library comes with an `example-project` folder with sample `gulpfile.js` and `main.scss` files to help you get started. For most projects, this would entail creating your own `.scss` file and declaring the variables from the Yale UI Component Library that you want to override. Below those declarations, import the Yale UI `main.scss` file.

## Libraries and Dependencies

### Bootstrap

This UI style guide is largely built on [Bootstrap](https://getbootstrap.com) 4.3. Where possible, we maintained or slightly altered Bootstrap's default styles. Because web accessibility is a strong driver for this project, we enhanced, reimplemented, or removed parts of Bootstrap that do not facilitate conformance with Yale's Web Accessibility Policy.

Noteworthy pieces of Bootstrap that we have not and may not include in the component library:

- Scrollspies
- Toasts

Developers who wish to recompile the UI Component Library to include these assets are responsible for ensuring they are usable and accessible.

#### Bootstrap Re-implementations

We have re-implemented the following parts of Bootstrap to be more appropriate for our purposes:

- Alerts
- Text and Background Utilities
- List Groups
- Buttons
- Tables
- Progress Bars

In all of these cases, we strove to keep the API broadly similar to Bootstrap, so users who are comfortable with Bootstrap will hopefully find the transition easy.

### Focus-Visible

[Focus-Visible](https://github.com/WICG/focus-visible) is a polyfill for the `:focus-visible` pseudo-selector. Browsers implement focus styles inconsistently, such that some browsers will display focus styles when mouse users click on a focusable element. Many designers and developers find that behavior unattractive, and they typically respond by removing focus styles altogether, creating an accessibility problem for users who rely on focus. This polyfill only shows focus styles to keyboard user, and hides from mouse users consistently across browsers.

### Sass Color Contrast Function

Because Bootstrap consistently presents color contrast issues, Yale has implemented a [Sass color contrast library](https://git.yale.edu/yale-ui/sass-color-contrast-functions) to force color contrast based on WCAG standards. The library is based on a [gist by Sérgio Gomes](https://gist.github.com/sgomes/9c03a58976b90e00f4172a510b9807fa). It is a best practice to pass all Sass color declarations through the `find-accessible-colors` function in order to ensure accessible colors.

## Version History and Changelog

### 2.4.0

- Removed Bootstrap.js from bundled JavaScript
- Removed Ally.js from bundled JavaScript

### 1.4.0

- Added a new layout example on Examples page
- Included more Card designs
- Added an Accordion design to replace the Collapse button

### 1.3.0

- Added spinner component.
- More distinct colors were chosen for Primary, Info, Secondary, and Light theme colors.
- Adjusted hover state of badge links to have sufficient color contrast.
- Adjusted focus indicator of forms and custom forms to be consistent with other focusable elements (to display black or white depending on the background color).
- Increased color contrast ratio of `<code>` elements and the required (asterisk) symbol to be more distinct from the body text around it (and has a contrast ratio of 3:1 with the black body text).

### 1.2.0

- Added components: tooltips/popovers, dropdowns, collapse, input groups, badges

### 1.1.11

- Reduced the top margin of headings
- Added fallback italic styles for italic fonts
- Included a dismissable alert example with accessibility notes

### 1.1.10

- Font class names updated to pull in the properly corresponding fonts
- Alert components styled (with border, color, icons)
- Added documentation on differences between Bootstrap and our library
- Added CSS to implement focus-visible
- Focus indicator updated to have increased color contrast depending on background color

### 1.1.9

- Added two example layouts on the Examples page
- Updated documentation on forms page to discourage use of input type=”number”
- Changed the gutter width back to the Bootstrap default of 30px
- Made several improvements to the documentation site navigation

### 1.1.8

- Bundled entire Bootstrap JS (instead of individual Boostrap JS files)

### 1.1.7

- Fixed npm package to include dist folder.
- Added MIT license.

### 1.1.6

- Moved source code and documentation site to GitLab and GitLab pages, respectively.

### 1.1.5

- Moved Scss files to dist folder to be included in downloadable zip file
- Removed fonts and dist folders from commit history

### 1.1.4

- Added Font Awesome v4.7 icon fonts back to flyout menu (included as CDN, with instructions)

### 1.1.3

- Created npm package
- Added Google Analytics to documentation site

### 1.1.2

- Removed fonts from fonts folder (also removed @font-face rules in CSS)
- Included instructions on how to add fonts
- Added compiled and minified CSS and JS files to a downloadable zip file

### 1.1.1

- Updated forms documentation with input types and HTML’s built-in validation features
- Scrollable regions are now keyboard-accessible
- Form input fields now have sufficient color contrast
- Removed Mallory font (referencing a local GitHub page that hosts the Mallory font instead)
- Removed local dependencies (yale-fonts and sass color contrast function) and added to src folder instead

### 1.1.0

- Updated color variables so that everything is accessible by default (sass color contrast function warning will not appear)
- Added Carousel component with documentation

### 1.0.1

- Fix responsive menus and flyout menu example on examples page

### 1.0.0

- Documentation Fixes
- Adding yarn sassy script

### 0.8.0

- Documentation for Nav and Navbar components
- Implementation and documentation for the Yale Accessible Flyout Menu
- Documentation for Progress Bars
- Documentation for Pagination
- Documentation for Media Object
- Documentation for Jumbotrons
- Documentation for Cards

### 0.7.0

- Add popper.js dependency
- Include nav and navbar from Bootstrap
- Include badges from Bootstrap
- Include cards from Bootstrap
- Include dropdowns from Bootstrap, and add dropdowns to the compiled JavaScript
- Inlucde jumbotron from Bootstrap
- Include media from Bootstrap
- Include progress from Bootstrap
- Simplify custom typography implementation
- Use navbar for documentation website
- Tweak default focus styles
- Tweak layout for documentation website

### 0.6.1

- Change Bootstrap dependency to 4.3.1 from ^4.3.1
- Add CSS autoprefixer to gulp tasks
- Tweaked button typography
- Add custom form elements

### v0.6.0

- Combine tables, list groups, yale-bootstrap-base, buttons, and utilities into main repo, rather than separate projects
- Update typography to be consistent with yale.edu
- Add pagination component
- Hard code CSS for flyout menus

### v0.5.4

- Hotfix: remove broken reference to yale flyout menu

### v0.5.3

- Tweaks to yale topper logo sizing
- Include Bootstrap Transitions
- More robust error message reporting for Sass color contrast functions

### v0.5.2

- Updates to topper typography color
- Skip Link added to documentation website

### v0.5.1

- Updates to Alerts and Buttons documentation
- Simplified the example gulpfile

### v0.5.0

- Overhaul documentation to make it more consistent
- Add documentation about extending, dependencies
- Refactor project to separate the Bootstrap adjustment from the Yale branding skin
- Update readme, documentation website homepage

### v0.4.0

- Rewrite the gulpfile to concatenate all of our vendor libraries.

### V0.3.0

- Add collapsible button for documentation example code snippets in example.html
- Tables documentation
- Alerts documentation
- Forms documentation for validation messages
- Bootstrap utilities documentation
- Modal dialogs documentation
- Simplify the implementation of `_images.scss`
- Images documentation
- List Groups documentation
- Breadcrumbs documentation

### v0.2.2

- Make the alerts link styles visually accessible by default (don't require `.alert-link` class for accessible links inside of alerts)

### v0.2.1

- Fixed a bug that caused documentation website pages and sidebar navigation to not be created effectively
- Fixed a bug that caused documentation website pages to have the sidebar crash into the main column
- Updated the readme file to point to the documentation website home page
